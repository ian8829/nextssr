import Link from 'next/link'
import Head from 'next/head'
import Layout from '../components/Layout'

const Index = () => (
    <Layout>
        <Head>
            <title>Home Page</title>
            <meta name="description" content="My SEO React app with Next JS" />
            <meta name="keywords" content="next react seo" />
            <meta name="author" content="Ryan Dhungel" />
        </Head>
        <h2>Hello from Next JS</h2>
        <Link href="/about">
            <a>About Page</a>
        </Link>
    </Layout>
)

export default Index