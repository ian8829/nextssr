import Link from 'next/link'
import Layout from '../components/Layout'

const myStyle = {
    color: 'red', 
    borderLeft: '5px solid black', 
    padding: '5px'
}

const About = () => (
    <Layout>
        <h2>About Page</h2>
        <Link href="/">
            <a style={myStyle}>Home Page</a>
        </Link>
        <p>
            Test Paragraph.....................
        </p>

        <style jsx>{`
            p {
                color: indigo;
                font-size: 20px;
            }
        `}</style>
    </Layout>
)

export default About